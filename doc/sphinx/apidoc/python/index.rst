.. _api_python:

********************
Python API Reference
********************

.. toctree::
   :maxdepth: 2

   sdurw.rst
   sdurw_assembly.rst
   sdurw_control.rst
   sdurw_pathoptimization.rst
   sdurw_pathplanners.rst
   sdurw_proximitystrategies.rst
   sdurw_simulation.rst
   sdurw_task.rst
   sdurws.rst
   sdurwsim.rst
